import os
import requests
import time
import re
from datetime import datetime
from bs4 import BeautifulSoup
import json


"""
{
    roomid:{
        'status': True or False,
        'id': int,
        'shortid': int,
        'title': string,
    },
    ...
    5441,322892; 47867,67141; 544893,13705279
}
"""
def get_latest_title():
    """从 status.json 中获取最近的 title
    """
    src = 'status.json'
    with open(src, 'rb') as f:
        data = json.load(fp=f)
    return data


# 每次重启的时候将title变量换为最后一次题目名称
title_stat = get_latest_title()

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'}

for roomid in title_stat:
    try:
        # 获取直播间标题
        title_api = 'https://api.live.bilibili.com/room/v1/Room/get_info?room_id={}&from=room'.format(roomid)
        r = requests.get(title_api, timeout=60, headers=headers)
        # B站偶尔会出现编码问题，强制设置为utf8
        r.encoding = 'utf8'
        tjson = r.json()
        t = tjson['data']['title']
        # 获取直播间关注人数
        s = requests.get('https://api.live.bilibili.com/relation/v1/Feed/GetUserFc?follow={}'.format(title_stat[roomid]['id']), timeout=60, headers=headers)
        curr_follow = str(s.json()['data']['fc'])
        # 直播状态更新
        if tjson['data']['live_status'] == 1:
            curr_stat = True
        else:
            curr_stat = False
        # 打印并记录在title.log文件里
        with open('title.log', 'a') as f:
            f.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' {}: {}\t{}\t{}\n'.format(title_stat[roomid]['shortid'], curr_stat, curr_follow, t))
        # 如果直播状态有改动便更新
        if(curr_stat != title_stat[roomid]['status']):
            title_stat[roomid]['status'] = curr_stat
            s_dict = {True: 'LIVE: 直播开始', False: 'LIVE: 下播了'}
            string = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ': ' + s_dict[curr_stat]
            with open('./public/{}/index.html'.format(title_stat[roomid]['shortid']), 'r') as f:
                lines = f.readlines()
            lines.insert(36, '\t\t\t\t\t\t\t\t\t\t<li class="list-item">' + string + '</li>\n')
            with open('./public/{}/index.html'.format(title_stat[roomid]['shortid']), 'w') as f:
                f.writelines(lines)
        # 如果直播标题有改动便更新标题
        if(t != title_stat[roomid]['title']):
            title_stat[roomid]['title'] = t
            string = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ': ' + t + ' (' + curr_follow + '关注)'
            with open('./public/{}/index.html'.format(title_stat[roomid]['shortid']), 'r') as f:
                lines = f.readlines()
            lines.insert(36, '\t\t\t\t\t\t\t\t\t\t<li class="list-item">' + string + '</li>\n')
            with open('./public/{}/index.html'.format(title_stat[roomid]['shortid']), 'w') as f:
                f.writelines(lines)
    except Exception as exc:
        err = '抛出了异常: %s' % exc
        print(err)

with open('status.json','w') as f:
    json.dump(obj=title_stat, fp=f)
